#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring JNICALL
Java_id_ac_ui_cs_mobileprogramming_hanifagungprayoga_asikbaca_MainActivity_stringFromJNI(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}
extern "C"
JNIEXPORT jint JNICALL
Java_id_ac_ui_cs_mobileprogramming_hanifagungprayoga_asikbaca_PeminjamanActivity_computeHargaFromJNI(
        JNIEnv *env, jobject thiz, jintArray array_of_hargas) {
    // TODO: implement computeHargaFromJNI()
    int i;
    int hasil = 0;
    jsize len = (*env).GetArrayLength(array_of_hargas);
    jint *body = (*env).GetIntArrayElements(array_of_hargas, 0);
    for (i = 0; i < len; i++) {
        hasil = hasil + body[i];
    }
    return hasil;
}