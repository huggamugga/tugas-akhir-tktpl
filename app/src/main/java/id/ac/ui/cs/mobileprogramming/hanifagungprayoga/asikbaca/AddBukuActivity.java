package id.ac.ui.cs.mobileprogramming.hanifagungprayoga.asikbaca;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import id.ac.ui.cs.mobileprogramming.hanifagungprayoga.asikbaca.ViewModels.BukuViewModel;
import id.ac.ui.cs.mobileprogramming.hanifagungprayoga.asikbaca.datas.buku.Buku;

import android.Manifest;

public class AddBukuActivity extends AppCompatActivity {
    private EditText editTextJudul;
    private EditText editTextPenulis;
    private EditText editTextKeterangan;
    private EditText editTextLokasi;
    private BukuViewModel bukuViewModel;
    private Button submit;
    private Button pilihGambar;
    private ImageView holderImage;
    private Uri selectedUri;
    private Button btnAutoLocation;

    private LocationManager mLocationManager;
    private ConnectivityManager connectivityManager;

    private EditText editTextHarga;

    final int REQUEST_CODE_GALLERY = 999;
    final int REQUEST_CODE_LOCATION = 998;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_buku);
        init();
        MediaManager.init(this);

        pilihGambar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(
                        AddBukuActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_CODE_GALLERY
                );
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            String cloudinaryUrl;

            @Override
            public void onClick(View v) {
                try {
                    NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                    if (networkInfo ==null || !networkInfo.isConnected()) {
                        createDialogNoConnection();
                    } else {
                        final String judul = editTextJudul.getText().toString();
                        final String lokasi = editTextLokasi.getText().toString();
                        final String penulis = editTextPenulis.getText().toString();
                        final String keterangan = editTextKeterangan.getText().toString();
                        final String imgStr = "stringggg";

                        final int harga = Integer.parseInt(editTextHarga.getText().toString());

                        Toast.makeText(getApplicationContext(), "Saving...", Toast.LENGTH_SHORT).show();

                        MediaManager.get().upload(selectedUri)
                                .unsigned("lltndmb8")
                                .option("resource_type", "image")
                                .callback(new UploadCallback() {
                                    @Override
                                    public void onStart(String requestId) {
                                        Log.d("CLOUDINARY", "onStart: starting.....");
                                    }

                                    @Override
                                    public void onProgress(String requestId, long bytes, long totalBytes) {

                                    }

                                    @Override
                                    public void onSuccess(String requestId, Map resultData) {
                                        cloudinaryUrl = resultData.get("secure_url").toString();
                                        Log.d("CLOUDINARY", "onSuccess: succeed");
                                        Log.d("CLOUDINARY", "onSuccess: " + cloudinaryUrl);

                                        bukuViewModel.insert(new Buku(judul, penulis, lokasi, keterangan, imgStr, cloudinaryUrl, harga));
                                    }

                                    @Override
                                    public void onError(String requestId, ErrorInfo error) {

                                    }

                                    @Override
                                    public void onReschedule(String requestId, ErrorInfo error) {

                                    }
                                }).dispatch();
                    }
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        btnAutoLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(AddBukuActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(AddBukuActivity.this,
                            Manifest.permission.ACCESS_FINE_LOCATION)) {
                        ActivityCompat.requestPermissions(AddBukuActivity.this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_LOCATION);
                    } else {
                        ActivityCompat.requestPermissions(AddBukuActivity.this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_LOCATION);
                    }
                } else {
                    Toast.makeText(AddBukuActivity.this, "Getting location...", Toast.LENGTH_SHORT).show();
                    Log.d("yang ini", "onClick: ");
                    mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1,
                            1, mLocationListener);
                }
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_GALLERY) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_CODE_GALLERY);
            } else {
                Toast.makeText(getApplicationContext(), "You don't have permission to access file location!", Toast.LENGTH_SHORT).show();
            }
            return;
        } else if (requestCode == REQUEST_CODE_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(AddBukuActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(AddBukuActivity.this, "Getting location...", Toast.LENGTH_SHORT).show();
                    mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1,
                            1, mLocationListener);
                }
            } else {
                createDialogLocationRefused();
                Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void createDialogLocationRefused() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.titleNotUsingLocation));
        alertDialogBuilder.setMessage(getString(R.string.bodyNotUsingLocation));
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void createDialogNoConnection() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.titleNoConnection));
        alertDialogBuilder.setMessage(getString(R.string.bodyNoConnection));
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK && data != null) {
            selectedUri = data.getData();

            try {
                InputStream inputStream = getContentResolver().openInputStream(selectedUri);

                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                holderImage.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void init() {
        bukuViewModel = new BukuViewModel(getApplication());
        editTextJudul = findViewById(R.id.input_judul);
        editTextPenulis = findViewById(R.id.input_penulis);
        editTextKeterangan = findViewById(R.id.input_keterangan);
        editTextLokasi = findViewById(R.id.input_lokasi);
        submit = findViewById(R.id.button_submit_buku);
        pilihGambar = findViewById(R.id.button_pilih_gambar);
        holderImage = findViewById(R.id.imgHolderUpload);
        btnAutoLocation = findViewById(R.id.btnAutoLocation);
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        editTextHarga = findViewById(R.id.input_harga);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Intent nowIntent = new Intent(this, this.getClass());
            startActivity(nowIntent);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Intent nowIntent = new Intent(this, this.getClass());
            startActivity(nowIntent);
        }
    }

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            //your code here
            String myAddr = getCompleteAddressString(location.getLatitude(), location.getLongitude());
//            String myLoc = "Latitude: " + location.getLatitude() +  "Longitude: " + location.getLongitude();
//            Toast.makeText(AddBukuActivity.this, myLoc, Toast.LENGTH_SHORT).show();
            editTextLokasi.setText(myAddr);
            mLocationManager.removeUpdates(this);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
//                Log.w("My Current loction address", strReturnedAddress.toString());
            } else {
//                Log.w("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
//            Log.w("My Current loction address", "Canont get Address!");
        }
        return strAdd;
    }
}
