package id.ac.ui.cs.mobileprogramming.hanifagungprayoga.asikbaca.datas.buku;


import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "buku")
public class Buku {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String judul;
    private String penulis;
    private String lokasi;
    private String keterangan;
    private String img_url;
    private String img_url_cloudinary;
    private int harga;


    public Buku(String judul, String penulis, String lokasi, String keterangan, String img_url, String img_url_cloudinary, int harga) {
        this.judul = judul;
        this.penulis = penulis;
        this.lokasi = lokasi;
        this.keterangan = keterangan;
        this.img_url = img_url;
        this.img_url_cloudinary = img_url_cloudinary;
        this.harga = harga;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getPenulis() {
        return penulis;
    }

    public void setPenulis(String penulis) {
        this.penulis = penulis;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getImg_url_cloudinary() {
        return img_url_cloudinary;
    }

    public void setImg_url_cloudinary(String img_url_cloudinary) {
        this.img_url_cloudinary = img_url_cloudinary;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }
}
