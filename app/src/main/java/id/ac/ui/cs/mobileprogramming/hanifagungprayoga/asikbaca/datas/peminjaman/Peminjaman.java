package id.ac.ui.cs.mobileprogramming.hanifagungprayoga.asikbaca.datas.peminjaman;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "peminjaman_self")
public class Peminjaman {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private String judul;
    private String penulis;
    private String img_url_cloudinary;
    private boolean confirmed;
    private int harga;

    public Peminjaman(String judul, String penulis, String img_url_cloudinary, boolean confirmed, int harga) {
        this.judul = judul;
        this.penulis = penulis;
        this.img_url_cloudinary = img_url_cloudinary;
        this.confirmed = confirmed;
        this.harga = harga;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getPenulis() {
        return penulis;
    }

    public void setPenulis(String penulis) {
        this.penulis = penulis;
    }

    public String getImg_url_cloudinary() {
        return img_url_cloudinary;
    }

    public void setImg_url_cloudinary(String img_url_cloudinary) {
        this.img_url_cloudinary = img_url_cloudinary;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }
}
