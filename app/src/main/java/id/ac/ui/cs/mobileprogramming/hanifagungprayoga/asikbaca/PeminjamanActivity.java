package id.ac.ui.cs.mobileprogramming.hanifagungprayoga.asikbaca;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.List;
;import id.ac.ui.cs.mobileprogramming.hanifagungprayoga.asikbaca.ViewModels.PeminjamanViewModel;
import id.ac.ui.cs.mobileprogramming.hanifagungprayoga.asikbaca.adapters.PeminjamanAdapter;
import id.ac.ui.cs.mobileprogramming.hanifagungprayoga.asikbaca.datas.peminjaman.Peminjaman;

public class PeminjamanActivity extends AppCompatActivity {

    static {
        System.loadLibrary("native-lib");
    }

    private PeminjamanViewModel peminjamanViewModel;

    private int HARGA_TOTAL;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.peminjaman);

        RecyclerView recyclerView = findViewById(R.id.recycler_view_homepage);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        final PeminjamanAdapter peminjamanAdapter = new PeminjamanAdapter();
        recyclerView.setAdapter(peminjamanAdapter);

        peminjamanViewModel = ViewModelProviders.of(this).get(PeminjamanViewModel.class);
        peminjamanViewModel.getAllPeminjamans().observe(this, new Observer<List<Peminjaman>>() {
            @Override
            public void onChanged(List<Peminjaman> bukus) {
                // update RecyclerView
                peminjamanAdapter.setPeminjamans(bukus);
            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {
                final TextView editHargaTotal = (TextView) findViewById(R.id.textViewHargaTotal);

                List<Peminjaman> bukusDipinjam = peminjamanViewModel.getAllPeminjamansAsList();

                int[] arrayOfHargas = new int[bukusDipinjam.size()];

                for (int i = 0; i < bukusDipinjam.size(); i++) {
                    arrayOfHargas[i] = bukusDipinjam.get(i).getHarga();
                }

                HARGA_TOTAL = computeHargaFromJNI(arrayOfHargas);

                editHargaTotal.post(new Runnable() {
                    @Override
                    public void run() {
                        editHargaTotal.setText("" + HARGA_TOTAL);
                    }
                });
            }
        }).start();
    }


    public native int computeHargaFromJNI(int[] arrayOfHargas);
}
