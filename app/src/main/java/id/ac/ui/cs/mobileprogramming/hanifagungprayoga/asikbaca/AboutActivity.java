package id.ac.ui.cs.mobileprogramming.hanifagungprayoga.asikbaca;

import android.opengl.GLSurfaceView;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import id.ac.ui.cs.mobileprogramming.hanifagungprayoga.asikbaca.OpenGLES.MyGLSurfaceView;

public class AboutActivity extends AppCompatActivity {

    private GLSurfaceView mGLView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        mGLView = new MyGLSurfaceView(this);
        setContentView(mGLView);

    }
}
